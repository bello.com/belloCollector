package belloCollector.test;

import java.time.DayOfWeek;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAdjusters;

import org.junit.Test;

/**
 * java 8 日期时间测试
 * @author admin
 *
 */
public class DateTest {

	@Test
	public void test() {
		// Format examples
		LocalDate date = LocalDate.now();
		// default format
		System.out.println("Default format of LocalDate=" + date);
		// specific format
		System.out.println(date.format(DateTimeFormatter.ofPattern("d::MMM::uuuu")));
		System.out.println(date.format(DateTimeFormatter.BASIC_ISO_DATE));

		LocalDateTime dateTime = LocalDateTime.now();
		// default format
		System.out.println("Default format of LocalDateTime=" + dateTime);
		// specific format
		System.out.println(dateTime.format(DateTimeFormatter.ofPattern("d::MMM::uuuu HH::mm::ss")));
		System.out.println(dateTime.format(DateTimeFormatter.BASIC_ISO_DATE));

		Instant timestamp = Instant.now();
		// default format
		System.out.println("Default format of Instant=" + timestamp);

		// Parse examples
		//LocalDateTime dt = LocalDateTime.parse("27::Apr::2014 21::39::48",DateTimeFormatter.ofPattern("d::MMM::uuuu HH::mm::ss"));
		//System.out.println("Default format after parsing = " + dt);
	}

	@Test
	public void testDate() {
		// 取当前日期：
		LocalDate today = LocalDate.now();
		System.out.println(today);
		// 根据年月日取日期，12月就是12：
		LocalDate crischristmas = LocalDate.of(2014, 12, 25);
		System.out.println(crischristmas);
		// 根据字符串取：
		LocalDate endOfFeb = LocalDate.parse("2014-02-28"); // 严格按照ISO yyyy-MM-dd验证，02写成2都不行，当然也有一个重载方法允许自己定义格式
		System.out.println(endOfFeb);
		//LocalDate.parse("2014-02-29"); // 无效日期无法通过：DateTimeParseException: Invalid date

		// 取本月第1天：
		LocalDate firstDayOfThisMonth = today.with(TemporalAdjusters.firstDayOfMonth());
		System.out.println("firstDayOfThisMonth = "+firstDayOfThisMonth);
		// 取本月第2天：
		LocalDate secondDayOfThisMonth = today.withDayOfMonth(2); 
		System.out.println("secondDayOfThisMonth = "+secondDayOfThisMonth);
		// 取本月最后一天，再也不用计算是28，29，30还是31：
		LocalDate lastDayOfThisMonth = today.with(TemporalAdjusters.lastDayOfMonth()); 
		System.out.println("lastDayOfThisMonth = "+lastDayOfThisMonth);
		// 取下一天：
		LocalDate firstDayOf2015 = lastDayOfThisMonth.plusDays(1); 
		System.out.println("firstDayOf2015 = "+firstDayOf2015);
		// 取2016年1月第一个周一，这个计算用Calendar要死掉很多脑细胞：
		LocalDate firstMondayOf2015 = LocalDate.parse("2016-01-01").with(TemporalAdjusters.firstInMonth(DayOfWeek.MONDAY)); 
		System.out.println("firstMondayOf2015 = "+firstMondayOf2015);
	}
	
	@Test
	public void testTime() {
		LocalTime nowTime = LocalTime.now();
		System.out.println("nowTime:"+nowTime);
		System.out.println("nowTimeWithNano:"+nowTime.withNano(0));
		System.out.println("nowTimeWithNano:"+nowTime.plusHours(2).withNano(0));
	}
}
